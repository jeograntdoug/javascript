const express = require('express');
const fs = require('fs');
const app = express();

const port = 3000;
const jsonDataFile = 'chatlines.json';

var msgList = [];
var clientList = {};
var count = 0;

loadAllData();
function saveAllData(){
	var dataStr = JSON.stringify({
		clientList: clientList,
		msgList: msgList
	}, null, " ");
	fs.writeFileSync(jsonDataFile, dataStr);
}

function loadAllData(){
	if(fs.existsSync(jsonDataFile)){
		var dataStr = fs.readFileSync(jsonDataFile);
		var data = JSON.parse(dataStr);
		msgList = data.msgList;
		clientList = data.clientList;
	}
}


app.use(express.static('public'));

app.get('/', (request,response) =>{
	response.sendFile('chatroomclient.html',{root: __dirname});
});
app.get('/stats', (request,response) =>{
	response.sendFile('chatroomstats.html',{root: __dirname});
});

app.get('/send',(request, response) =>{
    var name = request.query['name'];
    var color = decodeURIComponent(request.query['color']);
    var text = request.query['text'];
    
    console.log(`name:${name} color:${color} text:${text}`);
	if(name === '' || color === '' || text === ''){
		response.send('Invalid data');
		return;
	}
    msgList.push({
        name: name,
        color: color,
        text: text
    });
    if(clientList[name] == null){
        clientList[name] = {
            count: 1,
            textLength: text.length
        };
		console.log(`clientList[${name}]: count: 1 textLength: ${text.length}`);
    } else {
        var count = clientList[name].count++;
        var length = clientList[name].textLength += text.length;
		console.log(`count:${count} length:${length}`);
    }
    response.send('Message sent!');
	saveAllData();

	

//    console.log(clientList[name]);
//    console.log(JSON.stringify(clientList));
//    console.log(JSON.stringify(msgList));
});

app.get('/update',(request, response) =>{
    var html = "";
    for (m of msgList){
        html += `<option style="color:${m.color};" value=${m.name}>${m.name}: ${m.text}</option>`;
    }
    response.send(html);
});

app.get('/statistics',(request, response) =>{
	//console.log(clientList['name']);

    var html = "";
		for(client in clientList){
        //console.log(clientList[client]);

        html += `<tr><td>${client}</td><td>${clientList[client].count}</td><td>${clientList[client].textLength}</td></tr>`;
    }

    //console.log(JSON.stringify(msgList));
    response.send(html);
});


app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happended',err);
    }
    console.log(`server is listen on ${port}`);
});
