const express = require('express');
const fs = require('fs');
const app = express();

const port = 3000;
const jsonDataFile = 'trips.json';

var tripArray = [];
var airpArray = {};

function saveAllData() {
    var dataStr = JSON.stringify(tripArray, null, " ");
    fs.writeFileSync(jsonDataFile, dataStr);
}

app.get('/add', (request, response) => {
    var id = request.query['id'] - 0;
    var passport = request.query['passport'];
    var airpCode = request.query['airpCode'];
    var city = request.query['city'];
    var depDate = request.query['depDate'];
    var retDate = request.query['retDate'];

    //console.log("/add:" + JSON.stringify(request.query));

    if (isNaN(id)
            || !/^[A-Z]{2}[0-9]{6}$/.test(passport)
            || !/^[A-Z]{3}$/.test(airpCode)
            || city.length > 20 || city.length < 1
            || depDate == "" || retDate == "" || depDate > retDate) {

        response.send('Invalid Input');
        return;
    }

    tripArray.push({
        id: id,
        passport: passport,
        airpCode: airpCode,
        city: city,
        depDate: depDate,
        retDate: retDate
    });
    if (airpArray[airpCode] == null) {
        airpArray[airpCode] = {
            count: 1
        }
    } else {
        airpArray[airpCode].count++;
    }
    
    //console.log(JSON.stringify(tripArray));
    saveAllData();
    response.send(`Item ${id} added`);

});

app.get('/getlist', (request, response) => {
    var html = '';
    for (trip of tripArray) {
        html += `<option id=${trip.id}>${trip.id}: ${trip.passport} travels to ${trip.airpCode} on ${trip.city} from ${trip.depDate} to ${trip.retDate}</option>`
    }
    response.send(html);
});

app.get('/delete', (request, response) => {
    //console.log("/delete:" + JSON.stringify(request.query));
    var id = request.query['id'] - 0;
    if (isNaN(id)) {
        response.send(`id:${id} is invalid`);
    }

    for (var i = 0; i < tripArray.length; i++) {
        if (id == tripArray[i].id) {
            tripArray.splice(i, 1);
            response.send(`Item ${id} deleted`);
            break;
        }
    }
    //console.log(JSON.stringify(tripArray));
    saveAllData();
});

app.get('/getlistastable', (request, response) => {
    var html = '';
    var count = 0;
    for (airp in airpArray) {
        html += `<tr><td class='airpCode'>${airp}</td><td class='count'>${airpArray[airp].count}</td></tr>`
        if (count == 4) {
            break;
        }
    }
    //console.log(html);
    response.send(html);
});

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happended', err);
    }
    console.log(`server is listen on ${port}`);
});

