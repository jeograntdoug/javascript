const express = require('express');
const app = express();
const port = 3000;

var msgList = [];
var clientList = [];
var count = 0;

app.get('/send',(request, response) =>{
    var name = request.query['name'];
    var color = request.query['color'];
    var msg = request.query['msg'];
    
    msgList.push({
        name: name,
        color: color,
        msg: msg
    });
 
    if(clientList[name] == null){
        clientList[name] = {
            count: 1,
            msgLength: msg.length
        };
    } else {
        clientList[name].count;
        clientList[name].msgLength += msg.length;
    }
//    console.log(clientList[name]);
//    console.log(`name:${name} color:${color} msg:${msg}`);
//    console.log(JSON.stringify(clientList));
//    console.log(JSON.stringify(msgList));
    response.send('Message sent!');
});

app.get('/update',(request, response) =>{
    var html = "";
    for (m of msgList){
        html += `<option value=${m.name}>${m.name}: ${m.msg}</option>`;
    }
    response.send(html);
});

app.get('/statistics',(request, response) =>{
    var html = '<tr><th>name</th><th>Totalmsgs</th><th>Totalchars</th></tr>';
    for(client in clientList){ 
//        console.log(clientList[client]);
        html += `<tr><td>${client}</td><td>${clientList[client].count}</td><td>${clientList[client].msgLength}</td></tr>`;
    }
//    console.log(JSON.stringify(clientList));
//    console.log(JSON.stringify(msgList));
    response.send("<table>"+html+"</table>");
});


app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happended',err);
    }
    console.log(`server is listen on ${port}`);
});