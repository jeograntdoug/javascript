const express = require('express');
const app = express();
const port = 3000;

const fs = require('fs');


app.get('/', (request, response) => {
    response.sendFile('friendArray.html', {root: __dirname});
});
app.get('/hello', (request, response) => {
    response.send('Hello from Express!');
});

var friendArray = [];
app.get('/add', (request, response) => {
    var id = request.query['id'] - 0;
    var name = request.query['name'];
    var age = request.query['age'] - 0;

    if (isNaN(id) || isNaN(age)) {
        console.log(`id[${id}] and age[${age}] must be integer`);
        return;
    }
    if (!/[a-zA-Z -]{1,50}/.test(name)) {
        console.log('Name is invalid');
        return;
    }

    friendArray.push({
        id: id,
        name: name,
        age: age
    });
    
    response.send(`Person ${id} added`);
    
    var friendStr = `${id},${name},${age}\n`;
    fs.appendFile('friendlist.txt', friendStr, function(err) {

    if(err) {
        return console.log(err);
    }

    console.log("The file was saved!");
}); 

});

//FIXME
app.get('/delete', (request, response) => {
    var id = request.query['id'] - 0;
    if (isNaN(id)) {
        console.log('ID must be integer');
    }

    var index = friendArray.findIndex(function (friend) {
        return friend.id == id;
    });

    console.log(`id:${id} index:${index}`);
    response.send(`Person ${friendArray[index].id} deleted`);
    friendArray.splice(index, 1);
});

app.get('/fetchall', (request, response) => {
    html = '';
    console.log(JSON.stringify(friendArray));
    for(fr of friendArray){
        html += `<option value="${fr.id}"> ${fr.id}: ${fr.name} is ${fr.age} y/o`;
    }
    response.send(html);
});

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happended', err);
    }
    console.log(`server is listening on ${port}`);
});
