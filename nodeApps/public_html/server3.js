const express = require('express');
const app = express();
const port = 3000;

var randomList = [];
app.get('/genrand', (request, response) => {
    var max = request.query.max - 0;
    var min = request.query.min - 0;
    
    if(isNaN(max) || isNaN(min)){
    // do something;
    }
    var random = Math.floor( Math.random()*(max - min + 1) + min);
    response.send(random+"");
    randomList.push(random);
});

app.get('/history' , (request, response) => {
    var result = "";
    for(random of randomList){
        result += "<li>" + random + "</li>";
    }
    response.send(result);
});

var count = 0;

app.get('/count', (request, response) => {
    response.send('<p>Count is <b>' + ++count + '</b></p>');
});
app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }
    console.log(`server is listening on ${port}`);
});